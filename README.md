# An introduction to Keycloak for securing ASP.Net Core Web APIs

This project was adapted from https://github.com/stianst/keycloak-containers-demo

This project contains a three applications which together form a Web application which demostrates token based authentication using Keycloak.

Client Application: js-console

API: keycloakAPIDemo

Authentication Service: Keycloak 

The steps included here requires Docker and Postman.

## Start containers

### Create a user defined network

To make it easy to connect Keycloak to LDAP and the mail server create a user defined network:

    docker network create demo-network

### Start Keycloak

We're going to use an extended Keycloak image that includes a custom theme and some custom providers.

First, build the custom providers and themes with:

    mvn clean install

---
**NOTE**

You may have to install java and maven for this command to work

---

Then build the image with:
    
    docker build -t demo-keycloak -f keycloak/Dockerfile .

Finally run it with:

    docker run --name demo-keycloak -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin \
        -p 8080:8080 --net demo-network demo-keycloak

    
### Start JS Console application

The JS Console application provides a playground to play with tokens issued by Keycloak.

First build the image with:

    docker build -t demo-js-console js-console
    
Then run it with:

    docker run --name demo-js-console -p 8000:80 demo-js-console

### Start keycloakAPIDemo

Open in Visual Studio or VS Code

Run

## Creating the realm

Open [Keycloak Admin Console](http://localhost:8080/auth/admin/). Login with
username `admin` and password `admin`.

Create a new realm called `demo` (find the `add realm` button in the drop-down
in the top-left corner). 

Once created set a friendly Display name for the realm, for 
example `Demo SSO`.

## Create a client

Now create a client for the JS console by clicking on `clients` then `create`.

Fill in the following values:

* Client ID: `js-console`
* Click `Save`

On the next form fill in the following values:

* Valid Redirect URIs: `http://localhost:8000/*`
* Web Origins: `http://localhost:8000`

## Enable user registration

Let's enable user self-registration

Open the [Keycloak Admin Console](http://localhost:8080/auth/admin/). 

Click on `Realm settings` then `Login`.

Fill in the following values:

* User registration: `ON`

To try this out open the [JS Console](http://localhost:8000). 

You will be automatically redirected to the login screen. Click on `Register` 
and fill in the form.

You will then be able to login and the js-console will retrieve and display ID and Access tokens which can be used to access restricted data from the API

## Configure API

The keycloakAPIDemo application has been preconfigured to retrieve authentication information from the keycloak instance.

The weather forecast controller GET endpoint has been decorated with the [Authorize] attribute and will cause the client to be redirected to keycloak to get an access token

## Postman

An access token can be added to a request in Postman in the authenticate option. Select bearer token and copy the acces token from js-console

## Adding claims to the tokens

What if your applications want to know something else about users? Say you want to have
an avatar available for your users.

Keycloak makes it possible to add custom attributes to users as well as adding custom
claims to tokens.

First open `Users` and select the user you registered earlier. Click on attributes and the following attribute:

* Key: `avatar_url`
* Value: `https://www.keycloak.org/resources/images/keycloak_logo_480x108.png`

Click `Add` followed by `Save`.

Now Keycloak knows the users avatar, but the application also needs access to this. We're
going to add this through Client Scopes.

Click on `Client Scopes` then `Create`.

Fill in the following values:

* Name: `avatar`
* Consent Screen Text: `Avatar`

Click on `Save`. 

Click on `Mappers` then `Create`. Fill in the following values:

* Name: `avatar`
* Mapper Type: `User Attribute`
* User Attribute `avatar_url`
* Token Claim Name: `avatar_url`
* Claim JSON Type: `String`

Click `Save`.

Now we've created a client scope, but we also need to asign it to the client. Go to
`Clients` and select `js-console`. Select `Client Scopes`.

We're going to add it as a `Default Client Scope`. So select the `avatar` here and click
`Add selected`. As it's a default scope it is added to the token by default, if it's
set as an optional client scope the client has to explicitly request it with the `scope` 
parameter.

Now go back to the JS Console and click `Refresh`.


## Style that login

Perhaps you don't want the login screen to look like a Keycloak login screen, but rather 
add a splash of your own styling to it?

The Keycloak Docker image we built earlier actually contains a custom theme, so we 
have one ready to go.

Open the [Keycloak Admin Console](http://localhost:8080/auth/admin/). 

Click on `Realm Settings` then `Themes`. In the drop-down under `Login Theme` select
`sunrise`.

Try opening the [JS Console](http://localhost:8000) to login a take in the beauty of the
new login screen!

You may want to change it back before you continue ;).